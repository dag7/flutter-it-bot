import logging
import yaml

from google.cloud import firestore
from google.oauth2 import service_account

logger = logging.getLogger(__name__)

settings_filepath = "settings.yml"


class Settings:
    def __init__(self):
        credentials = service_account.Credentials.from_service_account_file(
            "../flutter-italia-service-account.json"
        )
        self.store = firestore.Client(credentials=credentials)

    def pope_stickers(self) -> list[str]:
        """
        List available stickers of Pope Francis
        To edit available stickers, edit src/settings.yml
        """
        with open(settings_filepath) as file:
            docs = yaml.load(file, Loader=yaml.FullLoader)

            stickers = docs["stickers"]["pope"]

            return stickers

    def available_options(self) -> list[str]:
        """
        List the available options.
        To edit available options, edit src/settings.yml
        """
        with open(settings_filepath) as file:
            docs = yaml.load(file, Loader=yaml.FullLoader)

            settings = docs["settings"]

            return settings

    def easter_egg(self, easter_egg_name):
        with open(settings_filepath) as file:
            docs = yaml.load(file, Loader=yaml.FullLoader)

            e_egg = docs["easter-eggs"][easter_egg_name]

            return e_egg

    def _settings_collection(self, group_id: int) -> firestore.DocumentReference:
        """ Returns the collection containing the settings for that group id """
        settings = self.store.collection("settings").document(str(group_id))
        return settings

    def toggle_property(self, setting: str, group_id: int):
        """ Toggle the value of the specified setting """
        toggled = not self.is_enabled(setting, group_id)
        self.set_property(setting, {"enabled": toggled}, group_id)

    def set_property(self, setting: str, value: dict, group_id: int) -> bool:
        """ Set the value of the indicated property """
        settings = self._settings_collection(group_id)

        settings_dict = settings.get().to_dict()
        if settings_dict is None:
            settings_dict = {}

        settings_dict[setting] = value

        settings.set(settings_dict)
        return True

    def is_enabled(self, setting: str, group_id: int) -> bool:
        """ Return the current status of a boolean settings """
        doc = self._settings_collection(group_id).get()

        if doc.to_dict() is None:
            self.set_property(setting, {"enabled": True}, group_id)
            return True

        # If the setting is not in the document, then it's true
        # Otherwise return the value of the setting
        return doc.to_dict().get(setting, {}).get("enabled", True)
