import logging

from google.cloud import firestore
from google.oauth2 import service_account

logger = logging.getLogger(__name__)


class Cheems:
    def __init__(self):
        credentials = service_account.Credentials.from_service_account_file(
            "../flutter-italia-service-account.json"
        )
        self.store = firestore.Client(credentials=credentials)

    def _bonk_collection(self) -> firestore.CollectionReference:
        return self.store.collection("cheems")

    def bonk(self, user_id: int, username: str, count: int = 1) -> None:
        doc = self._bonk_collection().document(str(user_id))

        bonks = self.get_bonks(user_id)

        doc.set({"bonks": bonks + count, "username": username})

    def get_bonks(self, user_id: int) -> int:
        doc = self._bonk_collection().document(str(user_id)).get().to_dict()

        if doc is None:
            return 0

        return int(doc.get("bonks", 0))

    def podium(self, count: int = 5) -> list[dict]:
        docs = (
            self._bonk_collection()
            .order_by("bonks", direction=firestore.Query.DESCENDING)
            .limit(count)
            .stream()
        )

        result = []
        for doc in docs:
            result.append(
                {
                    "user_id": doc.id,
                    "username": doc.to_dict().get("username", ""),
                    "bonks": doc.to_dict().get("bonks", 0),
                }
            )

        return result
