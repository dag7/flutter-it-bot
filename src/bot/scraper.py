import logging
import requests

from dataclasses import dataclass
from datetime import datetime

logger = logging.getLogger(__name__)

base_url = "https://flutteritalia.dev/wp-json"


@dataclass
class Post:
    title: str
    publish_date: str
    link: str


def query_posts(page: int = 1, per_page: int = 3) -> list[Post]:
    """ Query recent posts from flutteritalia.dev """
    posts_url = base_url + "/wp/v2/posts"
    url = f"{posts_url}?page={page}&per_page={per_page}"

    response = requests.get(url)

    if response.status_code != 200:
        if response.status_code == 404:
            logger.error(f"Error on GET {url}")
            return []

        logger.warning(
            f"GET {url}: Error on status code. Expected 200, Get {response.status_code}"
        )
        return []

    logger.info("GET posts, 200 OK")

    response.encoding = "utf-8"
    result = []

    for post in response.json():
        result.append(
            Post(
                title=post["title"]["rendered"],
                publish_date=format_date(post["date"]),
                link=post["link"],
            )
        )

    return result


def format_date(date: str) -> str:
    # "2021-03-18T21:02:12"
    dt = datetime.strptime(date, "%Y-%m-%dT%H:%M:%S")
    return dt.strftime("%d/%m/%Y")
