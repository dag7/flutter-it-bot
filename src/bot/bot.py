import functools
import logging
import i18n
import random
import re

from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import (
    CommandHandler,
    MessageHandler,
    Updater,
    CallbackContext,
    CallbackQueryHandler,
    Filters,
)

from datetime import date

from . import scraper
from .cheems import Cheems
from .settings import Settings

i18n.set("file_format", "yml")
i18n.set("locale", "it")
i18n.load_path.append("strings")

logger = logging.getLogger(__name__)

cheems = Cheems()
settings = Settings()


def run(token: str) -> None:
    """ Start the bot """
    updater = Updater(token)
    dispatcher = updater.dispatcher

    # Basic tools
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help))
    dispatcher.add_handler(CommandHandler("articoli", posts, run_async=True))

    # Fun tools
    dispatcher.add_handler(
        MessageHandler(
            Filters.regex(re.compile(r"\b(dart:)?dio\b", re.IGNORECASE))
            & ~Filters.command,
            pope,
        )
    )
    dispatcher.add_handler(CommandHandler("bonk", bonk, run_async=True))
    dispatcher.add_handler(CommandHandler("excalibonk", excalibonk, run_async=True))
    dispatcher.add_handler(CommandHandler("whatisbonk", whatisbonk))
    dispatcher.add_handler(CommandHandler("stats", bonk_stats))

    # Admin tools
    dispatcher.add_handler(CommandHandler("controlpanel", controlpanel, run_async=True))
    dispatcher.add_handler(CallbackQueryHandler(controlpanel_action))

    updater.start_polling()
    updater.idle()


#
# Decorators
#


def only_groups(func):
    def inner(u: Update, c: CallbackContext):
        # Ignore the message received at boot because it throws a useless exception
        if u.message is None:
            return

        chat = u.message.chat
        if chat.type == "group" or chat.type == "supergroup":
            logger.info("Function was called in a group or a supergroup")
            return func(u, c)
        logger.info("Function was not called in a group or a supergroup")

    return inner


def only_admins(func):
    @only_groups
    def inner(u: Update, c: CallbackContext):
        admins = u.message.chat.get_administrators()
        sender_id = u.message.from_user.id
        if any(sender_id == admin.user.id for admin in admins):
            logger.info("Function was called by an admin")
            return func(u, c)
        logger.info("Function was not called by an admin")

    return inner


def admin_query(func):
    def inner(u: Update, c: CallbackContext):
        logger.info("Entering admin_query")
        admins = u.callback_query.message.chat.get_administrators()
        sender_id = u.callback_query.from_user.id
        logger.info("Obtained info")

        if any(sender_id == admin.user.id for admin in admins):
            logger.info("Admin used button")
            return func(u, c)
        logger.info("Normal user used button")

    return inner


#
# Commands
#


def is_enabled(setting: str):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(u: Update, c: CallbackContext):
            if settings.is_enabled(setting, u.message.chat.id):
                func(u, c)
                logger.info(f"{setting} is enabled")
                return

            logger.info(f"{setting} is disabled")

        return wrapper

    return decorator


@only_groups
@is_enabled("dio")
def pope(update: Update, _: CallbackContext) -> None:
    """ Send a sticker of Pope Francis """
    random_pope_sticker_id = random.choice(settings.pope_stickers())
    update.message.reply_sticker(random_pope_sticker_id)


def help(update: Update, _: CallbackContext) -> None:
    """ List commands and their functions """
    chat = update.message.chat

    update.message.reply_markdown(i18n.t(f"{chat.type}.help"))


def posts(update: Update, _: CallbackContext) -> None:
    """ Query posts from flutteritalia.dev and send them in a formatted message """
    fi_posts = scraper.query_posts()

    text = i18n.t("common.posts") + "\n"

    for post in fi_posts:
        formatted_post = i18n.t(
            "common.templates.post",
            title=f"*{post.title}*",
            link=post.link,
            date=post.publish_date,
        )
        text += f"{formatted_post}\n"

    update.message.reply_markdown(text)


@only_groups
@is_enabled("bonk")
def bonk(update: Update, context: CallbackContext) -> None:
    """ Invoke cheems to send users to the horny jail """
    reply = update.message.reply_to_message
    chat = update.message.chat

    if reply is None:
        update.message.reply_markdown(i18n.t("common.bonk-usage"))
        return

    user = reply.from_user
    username = user.username if user.username is not None else user.first_name

    if update.message.from_user.id == user.id:
        update.message.reply_text(i18n.t("common.cannot-bonk.user-self", username=user))

    elif context.bot.id == user.id:
        update.message.reply_text(
            i18n.t("common.cannot-bonk.self", username=user.username)
        )

    else:
        cheems.bonk(user.id, username)

        bonks = cheems.get_bonks(user.id)

        user_ref = (
            f"@{user.username}"
            if user.username is not None
            else f"[{user.username}](tg://user?id={user.id})"
        )

        update.message.reply_markdown(
            i18n.t(f"{chat.type}.bonk", username=user_ref, count=bonks),
            reply_to_message_id=reply.message_id,
        )
        if bonks == 69:
            update.message.reply_photo(photo=settings.easter_egg("nice"))


@only_groups
@is_enabled("bonk")
def excalibonk(update: Update, context: CallbackContext) -> None:
    """ Invoke cheems to send users to the horny jail but using excalibonk """
    reply = update.message.reply_to_message
    chat = update.message.chat

    chat_data = context.chat_data
    today = str(date.today())
    last_used = chat_data.get("last_used", today)
    # This command can be used once a day
    if last_used == today:
        update.message.reply_markdown(i18n.t("common.excalibonk-cooldown"))
        return

    if reply is None:
        update.message.reply_markdown(i18n.t("common.excalibonk-usage"))
        return

    user = reply.from_user
    username = user.username if user.username is not None else user.first_name

    if update.message.from_user.id == user.id:
        update.message.reply_text(i18n.t("common.cannot-bonk.user-self", username=user))

    elif context.bot.id == user.id:
        update.message.reply_text(
            i18n.t("common.cannot-bonk.self", username=user.username)
        )

    else:
        random_number = random.randint(1, 100)
        threshold = 95

        if random_number <= threshold:
            return

        cheems.bonk(user.id, username, 5)
        chat_data["last_used"] = today

        bonks = cheems.get_bonks(user.id)

        user_ref = (
            f"@{user.username}"
            if user.username is not None
            else f"[{user.username}](tg://user?id={user.id})"
        )

        update.message.reply_markdown(
            i18n.t(f"{chat.type}.bonk", username=user_ref, count=bonks),
            reply_to_message_id=reply.message_id,
        )
        if bonks == 69:
            update.message.reply_photo(photo=settings.easter_egg("nice"))


@only_groups
@is_enabled("bonk")
def whatisbonk(update: Update, context: CallbackContext) -> None:
    update.message.reply_text("Baby don't bonk me, don't bonk me, no more")


@only_groups
def bonk_stats(update: Update, _: CallbackContext):
    podium_data = cheems.podium()

    podium = i18n.t("group.podium.title")

    for i, user in enumerate(podium_data):
        string_name = "group.podium.place"

        if i == 0:
            string_name = "group.podium.first-place"
        elif i == 1:
            string_name = "group.podium.second-place"
        elif i == 2:
            string_name = "group.podium.third-place"

        username = (
            user.get("username") if user.get("username") != "" else "Innominabile"
        )
        podium += i18n.t(
            string_name, username=username, number=i + 1, bonks=user.get("bonks", "")
        )

    update.message.reply_text(podium)


def control_keyboard(chat_id: int) -> list[list[InlineKeyboardButton]]:
    """ Create a keyboard with all settings and their state """
    keyboard = []
    avail_setting = settings.available_options()
    for s in avail_setting:
        emoji = "enabled" if settings.is_enabled(s, chat_id) else "disabled"

        keyboard.append(
            [
                InlineKeyboardButton(
                    i18n.t(f"group.controlpanel.{emoji}", setting=s), callback_data=s
                )
            ]
        )

    return keyboard


@only_groups
@only_admins
def controlpanel(update: Update, _: CallbackContext):
    """ Send a message with a keyboard to toggle settings """
    reply_markup = InlineKeyboardMarkup(control_keyboard(update.message.chat.id))
    update.message.reply_text(
        i18n.t("group.controlpanel.title"), reply_markup=reply_markup
    )


@admin_query
def controlpanel_action(update: Update, _: CallbackContext):
    """ Callback for controlpanel() """
    query = update.callback_query
    query.answer()

    if query.data in settings.available_options():
        settings.toggle_property(query.data, query.message.chat_id)
        reply_markup = InlineKeyboardMarkup(
            control_keyboard(update.callback_query.message.chat.id)
        )
        query.edit_message_reply_markup(reply_markup=reply_markup)
        return

    logger.info("User without privileges tried to use control panel")


def start(update: Update, _: CallbackContext) -> None:
    """ Welcome command """
    message = update.message
    chat = message.chat
    username = message.from_user.username

    welcome = i18n.t(f"{chat.type}.welcome", username=username, group_name=chat.title)
    description = i18n.t(f"{chat.type}.description")

    message.reply_markdown(i18n.t(f"{welcome} {description}"))
