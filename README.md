# Flutter OT Bot

Bot made for Flutter Italia Developers community

## Features
- [x] Greet the user
- [x] Get new articles from [flutteritalia.dev](https://flutteritalia.dev)
- [ ] Automatically send articles in groups
- [ ] Automatically send articles in private chats (There's a limitation so users must start a conversation with the bot first)

## Contribute
Feel free to contribute in any way you like: Issues, Features and ideas :smile:

### For developers
Create a file named .env and write that content
```
TOKEN="token-from-botfather"
```

> Get a token here: text to [BotFather](https://t.me/BotFather)

## External resources
- [Python-telegram-bot](https://python-telegram-bot.org)
