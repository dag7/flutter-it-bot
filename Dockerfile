FROM python:3.9-slim

WORKDIR /code

# Install required packages
ADD Pipfile* ./
RUN pip install pipenv && \
    pipenv install --system --deploy

# Copy the script
COPY src/ .

ENTRYPOINT [ "python", "./main.py" ]
